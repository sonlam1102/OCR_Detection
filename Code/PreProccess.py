import Code.mnist_reader as mnist

#Load Le Cunn 's MNIST data
#MNIST data are downloaded and saved to training_data folder. if exist downloading step will be ignored
#Return: training_Set X and training_set Y

#Author: Hoang Minh Quan - 14520725

class PreProccess:
    def getTrainData(self):
        X, Y, testX, testY = mnist.load_data(one_hot=True)
        X = X.reshape([-1, 28, 28, 1])
        return (X, Y)