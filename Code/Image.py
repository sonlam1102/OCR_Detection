import cv2
import numpy as np

#Author: Do Phu An - 14520002

font = cv2.FONT_HERSHEY_SIMPLEX
class Image:
    def __init__(self, imgagePath):
        self.img = imgagePath

    def findContour(self, element):
        threshold = cv2.adaptiveThreshold(element, 255, 0, 1, 19, 2)
        (_, contours, _) = cv2.findContours(threshold, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

        # init
        maxArea = 0
        maxContour = None

        for i in contours:
            area = cv2.contourArea(i)
            if area > 50:
                if area > maxArea:  # and len(approx)==4:
                    # biggest = approx
                    maxArea = area
                    maxContour = i

        if (maxArea == 0):
            return False
        return True

    def getNumberBox(self):
        inputImg = cv2.imread(self.img)
        blurredImg = cv2.GaussianBlur(inputImg, (15, 15), 0)
        grayImg = cv2.cvtColor(blurredImg, cv2.COLOR_BGR2GRAY)

        mask = np.zeros((grayImg.shape), np.uint8)
        kernel1 = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (11, 11))
        close = cv2.morphologyEx(grayImg, cv2.MORPH_CLOSE, kernel1)

        div = np.float32(grayImg) / (close)
        res = np.uint8(cv2.normalize(div, div, 0, 255, cv2.NORM_MINMAX))
        res2 = cv2.cvtColor(res, cv2.COLOR_GRAY2BGR)

        # adaptive threshold function is a function that have the same effect as our image segmentation function.
        # it means all 'light' points will be transformed into white points
        # and 'dark' points will be transformed into black ones
        threshold = cv2.adaptiveThreshold(res, 255, 0, 1, 19, 2)
        (_, contours, _) = cv2.findContours(threshold, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        # init
        biggest = None
        maxArea = 0
        maxContour = None

        for i in contours:
            area = cv2.contourArea(i)
            if area > 1000:
                if area > maxArea:  # and len(approx)==4:
                    maxArea = area
                    maxContour = i

        cv2.drawContours(mask, [maxContour], 0, 255, -1)
        cv2.drawContours(mask, [maxContour], 0, 0, 2)
        res = cv2.bitwise_and(res, mask)

        # =====================================
        kernelx = cv2.getStructuringElement(cv2.MORPH_RECT, (2, 10))

        dx = cv2.Sobel(res, cv2.CV_16S, 1, 0)
        dx = cv2.convertScaleAbs(dx)

        cv2.normalize(dx, dx, 0, 255, cv2.NORM_MINMAX)
        ret, close = cv2.threshold(dx, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
        close = cv2.morphologyEx(close, cv2.MORPH_DILATE, kernelx, iterations=1)

        (_, contour, _) = cv2.findContours(close, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        for cnt in contour:
            x, y, w, h = cv2.boundingRect(cnt)
            if h / w > 5:
                cv2.drawContours(close, [cnt], 0, 255, -1)
            else:
                cv2.drawContours(close, [cnt], 0, 0, -1)
        close = cv2.morphologyEx(close, cv2.MORPH_CLOSE, None, iterations=2)
        closex = close.copy()

        # ------------------------------------------------------
        # find horizontal lines
        kernely = cv2.getStructuringElement(cv2.MORPH_RECT, (10, 2))
        dy = cv2.Sobel(res, cv2.CV_16S, 0, 2)
        dy = cv2.convertScaleAbs(dy)
        cv2.normalize(dy, dy, 0, 255, cv2.NORM_MINMAX)
        ret, close = cv2.threshold(dy, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
        close = cv2.morphologyEx(close, cv2.MORPH_DILATE, kernely)

        (_, contour, _) = cv2.findContours(close, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        for cnt in contour:
            x, y, w, h = cv2.boundingRect(cnt)
            if w / h > 5:
                cv2.drawContours(close, [cnt], 0, 255, -1)
            else:
                cv2.drawContours(close, [cnt], 0, 0, -1)

        close = cv2.morphologyEx(close, cv2.MORPH_DILATE, None, iterations=2)
        closey = close.copy()

        # -------------------------------------------------------
        # after finding all vertical and horizontal lines, we will mix it by "AND" operator.
        bitwiseOrRes = cv2.bitwise_and(closex, closey)
        # cv2.imwrite("VerticalAndHorizontalLinesMix.png", bitwiseOrRes)

        img = inputImg.copy()

        # find the centroids. The collection of 'centroids' will be used to warp the input image to a flat-square image. ('square' in this case mean 'hinh vuong')
        (_, contour, _) = cv2.findContours(bitwiseOrRes, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
        centroids = []
        count = 0
        for cnt in contour:
            mom = cv2.moments(cnt)
            (x, y) = int(mom['m10'] / mom['m00']), int(mom['m01'] / mom['m00'])
            centroids.append((x, y))
            count += 1

        centroids = np.array(centroids, dtype=np.float32)
        c = centroids.reshape((count, 2))
        c2 = c[np.argsort(c[:, 1])]

        prevX = 0
        prevY = 0
        temp = []
        count = 1

        for i in range(len(c2)):
            x, y = c2[i]
            if (((x - prevX) ** 2 + (y - prevY) ** 2) ** 0.5 > 20):
                temp.append((x, y))
                (prevX, prevY) = (x, y)
                count += 1

        c2 = np.array(temp, dtype=np.float32)
        b = np.vstack([c2[i * 10:(i + 1) * 10][np.argsort(c2[i * 10:(i + 1) * 10, 0])] for i in range(10)])
        bm = b.reshape((10, 10, 2))

        output = np.zeros((450, 450, 3), np.uint8)

        # warp image, as same as warping image in panorama project.
        for i, j in enumerate(b):
            ri = int(i / 10)
            ci = int(i % 10)
            if ci != 9 and ri != 9:
                src = bm[ri:ri + 2, ci:ci + 2, :].reshape(4, 2)
                dst = np.array([[ci * 50, ri * 50], [(ci + 1) * 50 - 1,
                                ri * 50], [ci * 50, (ri + 1) * 50 - 1],
                                [(ci + 1) * 50 - 1, (ri + 1) * 50 - 1]], np.float32)
                retval = cv2.getPerspectiveTransform(src, dst)
                warp = cv2.warpPerspective(res2, retval, (450, 450))
                output[ri * 50:(ri + 1) * 50 - 1, ci * 50:(ci + 1) * 50 - 1] = warp[ri * 50:(ri + 1) * 50 - 1,ci * 50:(ci + 1) * 50 - 1].copy()

        input = []
        for i in range(0, 9):
            for j in range(0, 9):
                x1 = j * (output.shape[0] / 9) + 5
                x2 = (j + 1) * (output.shape[0] / 9) - 5
                y1 = i * (output.shape[1] / 9) + 5
                y2 = (i + 1) * (output.shape[1] / 9) - 5

                # this function to 'cut' the element from the sudoku matrix
                element = output[int(y1):int(y2), int(x1):int(x2)]
                count = count + 1
                if (element.size != 0):

                    element = cv2.cvtColor(element, cv2.COLOR_BGR2GRAY)
                    element = cv2.resize(element, (28, 28))
                    # ret, element = cv2.threshold(element, 127, 255, cv2.THRESH_BINARY_INV)

                    if self.findContour(element) == True:
                        blurredImg = cv2.GaussianBlur(element, (7, 7), 0)
                        element = cv2.adaptiveThreshold(blurredImg, 255, 0, 1, 9, 2)
                        input.append(element)
                        # cv2.imwrite(os.path.join('Image/extracted/', (str(count) + ".jpg")), element)
                    else:
                        input.append([])
        return input
